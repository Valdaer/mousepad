/* automatically generated from mousepad-window-ui.xml */
#ifdef __SUNPRO_C
#pragma align 4 (mousepad_window_ui)
#endif
#ifdef __GNUC__
static const char mousepad_window_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char mousepad_window_ui[] =
#endif
{
  "<ui><menubar name=\"main-menu\"><menu action=\"file-menu\"><menuitem ac"
  "tion=\"new\" /><menuitem action=\"new-window\" /><menuitem action=\"tem"
  "plate-menu\" /><separator /><menuitem action=\"open\" /><menu action=\""
  "recent-menu\"><menuitem action=\"no-recent-items\" /><placeholder name="
  "\"placeholder-recent-items\" /><separator /><menuitem action=\"clear-re"
  "cent\" /></menu><separator /><menuitem action=\"save\" /><menuitem acti"
  "on=\"save-as\" /><menuitem action=\"save-all\" /><menuitem action=\"rev"
  "ert\" /><separator /><menuitem action=\"print\" /><separator /><menuite"
  "m action=\"detach\" /><separator /><menuitem action=\"close\" /><menuit"
  "em action=\"close-window\" /></menu><menu action=\"edit-menu\"><menuite"
  "m action=\"undo\" /><menuitem action=\"redo\" /><separator /><menuitem "
  "action=\"cut\" /><menuitem action=\"copy\" /><menuitem action=\"paste\""
  " /><menu action=\"paste-menu\"><menuitem action=\"paste-history\" /><me"
  "nuitem action=\"paste-column\" /></menu><menuitem action=\"delete\" /><"
  "separator /><menuitem action=\"select-all\" /><menuitem action=\"change"
  "-selection\" /><separator /><menu action=\"convert-menu\"><menuitem act"
  "ion=\"lowercase\" /><menuitem action=\"uppercase\" /><menuitem action=\""
  "titlecase\" /><menuitem action=\"opposite-case\" /><separator /><menuit"
  "em action=\"tabs-to-spaces\" /><menuitem action=\"spaces-to-tabs\" /><s"
  "eparator /><menuitem action=\"strip-trailing\" /><separator /><menuitem"
  " action=\"transpose\" /></menu><menu action=\"move-menu\"><menuitem act"
  "ion=\"line-up\" /><menuitem action=\"line-down\" /></menu><menuitem act"
  "ion=\"duplicate\" /><menuitem action=\"increase-indent\" /><menuitem ac"
  "tion=\"decrease-indent\" /><separator /><menuitem action=\"preferences\""
  " /></menu><menu action=\"search-menu\"><menuitem action=\"find\" /><men"
  "uitem action=\"find-next\" /><menuitem action=\"find-previous\" /><menu"
  "item action=\"replace\" /><separator /><menuitem action=\"go-to\" /></m"
  "enu><menu action=\"view-menu\"><menuitem action=\"font\" /><separator /"
  "><menu action=\"color-scheme-menu\"><placeholder name=\"placeholder-col"
  "or-scheme-items\" /></menu><menuitem action=\"line-numbers\" /><separat"
  "or /><menuitem action=\"menubar\" /><menuitem action=\"toolbar\" /><men"
  "uitem action=\"statusbar\" /><separator /><menuitem action=\"fullscreen"
  "\" /></menu><menu action=\"document-menu\"><menuitem action=\"word-wrap"
  "\" /><menuitem action=\"auto-indent\" /><menu action=\"tab-size-menu\">"
  "<placeholder name=\"placeholder-tab-items\" /><separator /><menuitem ac"
  "tion=\"insert-spaces\" /></menu><separator /><menu action=\"language-me"
  "nu\"><placeholder name=\"placeholder-language-section-items\" /></menu>"
  "<menu action=\"eol-menu\"><menuitem action=\"unix\" /><menuitem action="
  "\"mac\" /><menuitem action=\"dos\" /></menu><separator /><menuitem acti"
  "on=\"write-bom\" /><separator /><menuitem action=\"back\" /><menuitem a"
  "ction=\"forward\" /><separator /><placeholder name=\"placeholder-file-i"
  "tems\" /></menu><menu action=\"help-menu\"><menuitem action=\"contents\""
  " /><menuitem action=\"about\" /></menu></menubar><toolbar action=\"main"
  "-toolbar\"><toolitem action=\"new\" /><toolitem action=\"open\" /><tool"
  "item action=\"save\" /><toolitem action=\"save-as\" /><toolitem action="
  "\"revert\" /><toolitem action=\"close\" /><separator /><toolitem action"
  "=\"undo\" /><toolitem action=\"redo\" /><toolitem action=\"cut\" /><too"
  "litem action=\"copy\" /><toolitem action=\"paste\" /><separator /><tool"
  "item action=\"find\" /><toolitem action=\"replace\" /><toolitem action="
  "\"go-to\" /><separator name=\"spacer\" /><toolitem action=\"fullscreen\""
  " /></toolbar><popup action=\"tab-menu\"><menuitem action=\"save\" /><me"
  "nuitem action=\"save-as\" /><separator /><menuitem action=\"revert\" />"
  "<separator /><menuitem action=\"detach\" /><menuitem action=\"close\" /"
  "></popup><popup action=\"textview-menu\"><menuitem action=\"undo\" /><m"
  "enuitem action=\"redo\" /><separator /><menuitem action=\"cut\" /><menu"
  "item action=\"copy\" /><menuitem action=\"paste\" /><menu action=\"past"
  "e-menu\"><menuitem action=\"paste-history\" /><menuitem action=\"paste-"
  "column\" /></menu><menuitem action=\"delete\" /><separator /><menuitem "
  "action=\"select-all\" /><menuitem action=\"change-selection\" /><separa"
  "tor /><menu action=\"convert-menu\"><menuitem action=\"lowercase\" /><m"
  "enuitem action=\"uppercase\" /><menuitem action=\"titlecase\" /><menuit"
  "em action=\"opposite-case\" /><separator /><menuitem action=\"tabs-to-s"
  "paces\" /><menuitem action=\"spaces-to-tabs\" /><separator /><menuitem "
  "action=\"strip-trailing\" /><separator /><menuitem action=\"transpose\""
  " /></menu><menu action=\"move-menu\"><menuitem action=\"line-up\" /><me"
  "nuitem action=\"line-down\" /></menu><menuitem action=\"duplicate\" /><"
  "menuitem action=\"increase-indent\" /><menuitem action=\"decrease-inden"
  "t\" /><separator name=\"menubar-visible-separator\" /><menuitem name=\""
  "menubar-visible-item\" action=\"menubar\" /></popup></ui>"
};

static const unsigned mousepad_window_ui_length = 4623u;

